import React, { Component } from "react";
import Details from "./views/Details";
import Home from "./views/Home";
import Signin from "./views/Signin";
import Signup from "./views/Signup";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import { AuthRoute, PrivateRoute } from "./HOCS/Route";
import Header from "./components/Header";
import { connect } from "react-redux";
import { fetchMe } from "./store/actions/auth";
import Profile from "./views/Profile";

class App extends Component {
  render() {
    return (
      <BrowserRouter>
        <Header />
        <Switch>
          <Route exact path="/" component={Home} />
          <Route path="/details/:id" component={Details} />
          <Route path="/signin" component={Signin} />
          <Route path="/signup" component={Signup} />
          <Route path="/profile" component={Profile} />
        </Switch>
      </BrowserRouter>
    );
  }
  componentDidMount() {
    const token = localStorage.getItem("t");
    if (token) {
      this.props.dispatch(fetchMe);
    }
  }
}

export default connect()(App);
