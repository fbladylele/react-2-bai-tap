import React, { Component, Fragment } from "react";
import { alpha, makeStyles } from "@material-ui/core/styles";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import IconButton from "@material-ui/core/IconButton";
import { Typography, Button } from "@material-ui/core";
import InputBase from "@material-ui/core/InputBase";
import Badge from "@material-ui/core/Badge";
import MenuItem from "@material-ui/core/MenuItem";
import Menu from "@material-ui/core/Menu";
import MenuIcon from "@material-ui/icons/Menu";
import SearchIcon from "@material-ui/icons/Search";
import AccountCircle from "@material-ui/icons/AccountCircle";
import MailIcon from "@material-ui/icons/Mail";
import NotificationsIcon from "@material-ui/icons/Notifications";
import MoreIcon from "@material-ui/icons/MoreVert";
import TheatersOutlinedIcon from "@material-ui/icons/TheatersOutlined";
import { NavLink } from "react-router-dom";
import Signin from "../../../views/Signin";
import Signup from "../../../views/Signup";
import { signIn } from "../../../store/actions/auth";

const useStyles = makeStyles((theme) => ({
  grow: {
    flexGrow: 1,
  },
  menuButton: {
    marginRight: theme.spacing(1),
  },
  title: {
    display: "none",
    [theme.breakpoints.up("sm")]: {
      display: "block",
    },
  },
  search: {
    position: "relative",
    borderRadius: theme.shape.borderRadius,
    backgroundColor: alpha(theme.palette.common.white, 0.15),
    "&:hover": {
      backgroundColor: alpha(theme.palette.common.white, 0.25),
    },
    marginRight: theme.spacing(2),
    marginLeft: 0,
    width: "100%",
    [theme.breakpoints.up("sm")]: {
      marginLeft: theme.spacing(3),
      width: "auto",
    },
  },
  searchIcon: {
    padding: theme.spacing(0, 2),
    height: "100%",
    position: "absolute",
    pointerEvents: "none",
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  inputRoot: {
    color: "inherit",
  },
  inputInput: {
    padding: theme.spacing(1, 1, 1, 0),
    // vertical padding + font size from searchIcon
    paddingLeft: `calc(1em + ${theme.spacing(4)}px)`,
    transition: theme.transitions.create("width"),
    width: "100%",
    [theme.breakpoints.up("md")]: {
      width: "20ch",
    },
  },
  sectionDesktop: {
    display: "none",

    [theme.breakpoints.up("md")]: {
      display: "flex",
    },
  },
  sectionMobile: {
    display: "flex",
    [theme.breakpoints.up("md")]: {
      display: "none",
    },
  },
}));

export default function PrimarySearchAppBar(props) {
  const classes = useStyles();
  const [anchorEl, setAnchorEl] = React.useState(null);
  const [mobileMoreAnchorEl, setMobileMoreAnchorEl] = React.useState(null);

  const isMenuOpen = Boolean(anchorEl);
  const isMobileMenuOpen = Boolean(mobileMoreAnchorEl);

  const handleProfileMenuOpen = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleMobileMenuClose = () => {
    setMobileMoreAnchorEl(null);
  };

  const handleMenuClose = () => {
    setAnchorEl(null);
    handleMobileMenuClose();
  };

  const handleMobileMenuOpen = (event) => {
    setMobileMoreAnchorEl(event.currentTarget);
  };

  const menuId = "primary-search-account-menu";

  const mobileMenuId = "primary-search-account-menu-mobile";
  const renderMobileMenu = (
    <Menu
      anchorEl={mobileMoreAnchorEl}
      anchorOrigin={{ vertical: "top", horizontal: "right" }}
      id={mobileMenuId}
      keepMounted
      transformOrigin={{ vertical: "top", horizontal: "right" }}
      open={isMobileMenuOpen}
      onClose={handleMobileMenuClose}
    >
      {!props.me ? (
        <>
          <MenuItem>
            <IconButton aria-label="show 4 new mails" color="inherit">
              <Badge badgeContent={"meomeo"} color="secondary">
                <MailIcon />
              </Badge>
            </IconButton>
            <p
              data-toggle="modal"
              data-target="#modelId"
              className="w-100 m-auto"
            >
              Đăng nhập
            </p>
          </MenuItem>
          <MenuItem>
            <IconButton aria-label="show 11 new notifications" color="inherit">
              <Badge badgeContent={"leuleu"} color="secondary">
                <NotificationsIcon />
              </Badge>
            </IconButton>
            <p
              data-toggle="modal"
              data-target="#modelId2"
              className="w-100 m-auto"
            >
              Đăng ký
            </p>
          </MenuItem>
        </>
      ) : (
        <MenuItem onClick={handleProfileMenuOpen}>
          <IconButton
            aria-label="account of current user"
            aria-controls="primary-search-account-menu"
            aria-haspopup="true"
            color="inherit"
          >
            <AccountCircle />
          </IconButton>
          <p style={{ marginTop: 15, color: "red" }}>
            Xin chào: {props.me.content.taiKhoan}
          </p>
        </MenuItem>
      )}
    </Menu>
  );

  return (
    <div className={classes.grow}>
      <AppBar
        position="static"
        style={{ backgroundColor: "#37383c", padding: "15px" }}
      >
        <Toolbar>
          {/* LOGO */}
          <NavLink
            style={{
              textDecoration: "none",
              width: "60%",
              color: "white",
              display: "flex",
            }}
            exact
            to="/"
          >
            <Typography className={classes.title} variant="h3" noWrap>
              M
            </Typography>
            <TheatersOutlinedIcon style={{ fontSize: 47, paddingTop: 3 }} />
            <Typography className={classes.title} variant="h3" noWrap>
              VIE
            </Typography>
          </NavLink>

          {/* <div className={classes.search}>
            <div className={classes.searchIcon}>
              <SearchIcon />
            </div>
            <InputBase
              placeholder="Search…"
              classes={{
                root: classes.inputRoot,
                input: classes.inputInput,
              }}
              inputProps={{ "aria-label": "search" }}
            />
          </div> */}

          <div className={classes.grow} />
          {/* header bự */}
          <div className={classes.sectionDesktop}>
            {!props.me ? (
              <Fragment>
                <IconButton aria-label="show 4 new mails" color="inherit">
                  <p
                    data-toggle="modal"
                    data-target="#modelId"
                    className="w-100 m-auto"
                  >
                    Đăng nhập
                  </p>
                </IconButton>
                <IconButton
                  aria-label="show 17 new notifications"
                  color="inherit"
                >
                  <Badge badgeContent={"welcome"} color="secondary">
                    {/* <NotificationsIcon /> */}
                    <p
                      data-toggle="modal"
                      data-target="#modelId2"
                      className="w-100 m-auto"
                    >
                      Đăng ký
                    </p>
                  </Badge>
                </IconButton>
              </Fragment>
            ) : (
              <Fragment>
                <NavLink
                  exact
                  activeClassName={props.active}
                  className={props.nav}
                  to="/"
                >
                  Home
                </NavLink>

                <NavLink
                  to="/profile"
                  style={{
                    textDecoration: "none",
                    color: "#988856",
                  }}
                >
                  <IconButton
                    edge="end"
                    aria-label="account of current user"
                    aria-controls={menuId}
                    aria-haspopup="true"
                    onClick={handleProfileMenuOpen}
                    color="inherit"
                  >
                    <AccountCircle
                      style={{
                        marginRight: 8,
                      }}
                    />
                    Hi, {props.me.content.taiKhoan}
                  </IconButton>
                </NavLink>
              </Fragment>
            )}

            <Signin/>
            <Signup />
          </div>
          <div className={classes.sectionMobile}>
            <IconButton
              aria-label="show more"
              aria-controls={mobileMenuId}
              aria-haspopup="true"
              onClick={handleMobileMenuOpen}
              color="inherit"
            >
              <MoreIcon />
            </IconButton>
          </div>
        </Toolbar>
      </AppBar>
      {renderMobileMenu}
    </div>
  );
}
