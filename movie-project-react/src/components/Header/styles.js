
const styles = (theme) => {
  return {
    active: {
      opacity: 1,
      color: "#ffffff",
    },
    nav: {
      textDecoration: "none !important",
      listStyle: "none",
      opacity: 0.75,
      color: "#ffffff",
      margin: 10,
      fontSize: 25,
      "&:hover": {
        color: "#988856",
        opacity: 1,
      },

    },
    
  };
};
export default styles;
