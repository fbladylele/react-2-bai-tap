import React, { Component } from "react";
import HeaderMUI from "./../Header/HeaderMUI/index";
import { connect } from "react-redux";
import styles from "./styles";
import { withStyles } from "@material-ui/core";

class Header extends Component {
  render() {
    const { active, nav, form } = this.props.classes;
    return (
      <div>
        <HeaderMUI me={this.props.me} active={active} nav={nav}/>
      </div>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    me: state.meReducer,
  };
};
export default connect(mapStateToProps)(
  withStyles(styles, { withTheme: true })(Header)
);
