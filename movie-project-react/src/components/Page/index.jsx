import { Button } from "@material-ui/core";
import React, { Component } from "react";
import { connect } from "react-redux";
import { fetchMovies } from "../../store/actions/movie";

class Page extends Component {
  //hàm chuyển trang
  handleChangePage = (page) => {
    this.props.dispatch(fetchMovies(page));
  };

  renderPages = () => {
    // tạo [chứa số trang]:
    let pageNumbers = [];
    let totalPosts = this.props.totalPages;
    let postsPerPage = this.props.count;

    for (let i = 1; i <= Math.ceil(totalPosts / postsPerPage); i++) {
      pageNumbers.push(i);
    }

    //lọc số trang nhả về các button số
    return pageNumbers.map((number) => (
      <li
        key={number}
        style={{
          display: "flex",
          alignItems: "spaceBetween",
          justifyContent: "center",
        }}
      >
        <Button
          size="small"
          onClick={() => this.handleChangePage(number)} //nhấn nút: call api theo số trang = tham số trong api
          style={{ textDecoration: "none", fontSize: 15, color: "white"}}
        >
          {number}
        </Button>
      </li>
    ));
  };

  render() {
    return (
      <nav
        style={{ margin: "auto", width: "100%", backgroundColor: "#37383c" }}
      >
        <ul
          style={{
            display: "flex",
            justifyContent: "center",
            paddingLeft: 0,
            listStyle: "none",
          }}
        >
          {this.renderPages()}
        </ul>
      </nav>
    );
  }
}

export default connect()(Page);
