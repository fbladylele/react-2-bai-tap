export const actionType = {
  SHOW_DETAILS: "SHOW_DETAILS",
  SET_MOVIE: "SET_MOVIE",
  SET_PROFILE: "SET_PROFILE",
};
