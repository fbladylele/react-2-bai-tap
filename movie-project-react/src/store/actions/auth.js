// dispatch for signin:
import axios from "axios";
import { createAction } from ".";
import { actionType } from "./type";

export const signIn = (userLogin, callback) => {
  return async (dispatch) => {
    try {
      const res = await axios({
        method: "POST",
        url: "http://movieapi.cyberlearn.vn/api/QuanLyNguoiDung/DangNhap",
        data: userLogin,
      });

      alert("lên b.e thành công");
      console.log(res);

      dispatch(createAction(actionType.SET_PROFILE, res.data));
      alert("lên store thành công");

      //ấn submit->dispatch xong-> cập nhật state -> b.e cho 1 accesstoken cùng lúc trong state. nhưng khi f5, data biến mất khỏi state: ko duy trì đc trạng thái signIn-> lưu  ngay token vào storage:
      localStorage.setItem("t", res.data.content.accessToken);
      callback();
      //
    } catch (err) {
      console.log("api called failed", err);
      // console.log({ ...err });
      // console.log(err.response.data.content);
      // alert(err.response.data.content);
    }
  };
};

//
export const fetchMe = async (dispatch) => {
  //call api lên b.e
  try {
    const res = await axios({
      url: "http://movieapi.cyberlearn.vn/api/QuanLyNguoiDung/ThongTinTaiKhoan",
      method: "POST",
      headers: {
        Authorization: "Bearer " + localStorage.getItem("t"),
      },
    });
    //b.e tra về info
    console.log(res);
    //dispatch lên store set lại state:
    dispatch(createAction(actionType.SET_PROFILE, res.data));
  } catch (err) {
    console.log(err);
  }
};
