//code dispatch gửi qua home gửi lên mdlw, movielist
import axios from "axios";
import { createAction } from ".";
import { actionType } from "./type";

export const fetchMovies = (page) => {
  return async (dispatch) => {
    try {
      const res = await axios({
        url: "https://movie0706.cybersoft.edu.vn/api/QuanLyPhim/LayDanhSachPhimPhanTrang",
        method: "GET",
        params: {
          maNhom: "GP01",
          soTrang: page,
          soPhanTuTrenTrang: 10,
        },
      });
      console.log("fetchMovies",res.data);
      dispatch(createAction(actionType.SET_MOVIE, res.data));
    } catch (err) {
      console.log("call api failed", err);
    }
  };
};

//for movie details
export const fetchMovie = (id) => {
  return async (dispatch) => {
    try {
      const res = await axios({
        url: "https://movie0706.cybersoft.edu.vn/api/QuanLyPhim/LayThongTinPhim",
        method: "GET",
        params: {
          maPhim: id,
        },
      });
      console.log("dispatch nè", res.data);
      dispatch(createAction(actionType.SHOW_DETAILS, res.data));
    } catch (err) {
      console.log(err);
    }
  };
};
