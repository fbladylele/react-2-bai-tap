import { actionType } from "../actions/type";

const initialState = {
  movies: [],
  movieDetails: null, // luc dau call api chua co dât thi no null là dung roi
  // nhưng sao có lúc nó chạy
  // nãy log dc mà
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case actionType.SET_MOVIE:
      state.movies = action.payload;
      console.log(action.payload);
      return { ...state };
    case actionType.SHOW_DETAILS:
      state.movieDetails=action.payload;
      console.log("setstate xong trong reducer rồi nè", state.movieDetails);
      return {...state}

    default:
      return state;
  }
};
export default reducer;
