import { Button, TextField, withStyles } from "@material-ui/core";
import axios from "axios";
import React, { Component } from "react";
import { fetchMovies } from "../../store/actions/movie";
import styles from "./styles";

class Signup extends Component {
  constructor(props) {
    super(props);
    this.state = {
      formValue: {
        taiKhoan: "",
        matKhau: "",
        hoTen: "",
        email: "",
        soDt: "",
        maNhom: "GP01",
      },
    };
  }

  handleChange = (event) => {
    console.log(event.target.value);
    this.setState({
      formValue: {
        ...this.state.formValue,
        [event.target.name]: event.target.value,
      },
    });
  };

  handleSubmit = async (event) => {
    event.preventDefault();
    console.log(this.state.formValue);

    try {
      const res = await axios({
        method: "POST",
        url: "http://movieapi.cyberlearn.vn/api/QuanLyNguoiDung/DangKy",
        data: this.state.formValue,
      });
      alert("yeah");
      console.log("đăng ký thành công", res);
      
    } catch (err) {
      console.log(err);
      alert("sai");
    }
  };

  render() {
    const { formInput } = this.props.classes;
    return (
      <div
        className="modal fade"
        id="modelId2"
        tabIndex={-1}
        role="dialog"
        aria-labelledby="modelTitleId"
        aria-hidden="true"
      >
        <div className="modal-dialog modal-md" role="document">
          <div className="modal-content">
            <div className="modal-header">
              <h3 className="text-dark text-center">Đăng Ký</h3>
              <button
                type="button"
                className="close"
                data-dismiss="modal"
                aria-label="Close"
              >
                <span aria-hidden="true">×</span>
              </button>
            </div>
            <div className="modal-body">
              <form onSubmit={this.handleSubmit}>
                <div className={formInput}>
                  <TextField
                    name="taiKhoan"
                    onChange={this.handleChange}
                    fullWidth
                    label="Tài khoản"
                    variant="outlined"
                  />
                </div>
                <div className={formInput}>
                  <TextField
                    name="matKhau"
                    type="password"
                    onChange={this.handleChange}
                    fullWidth
                    label="Mật khẩu"
                    variant="outlined"
                  />
                </div>
                <div className={formInput}>
                  <TextField
                    name="hoTen"
                    onChange={this.handleChange}
                    fullWidth
                    label="Họ Tên"
                    variant="outlined"
                  />
                </div>
                <div className={formInput}>
                  <TextField
                    name="email"
                    onChange={this.handleChange}
                    fullWidth
                    label="Email"
                    variant="outlined"
                  />
                </div>
                <div className={formInput}>
                  <TextField
                    name="soDt"
                    onChange={this.handleChange}
                    fullWidth
                    label="Số ĐT"
                    variant="outlined"
                  />
                </div>

                <div>
                  <Button type="submit" variant="contained" color="primary">
                    Đăng Ký
                  </Button>
                  <Button
                    type="button"
                    className="btn btn-secondary"
                    data-dismiss="modal"
                  >
                    Đóng
                  </Button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default withStyles(styles)(Signup);
