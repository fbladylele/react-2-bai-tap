import React, { Component } from "react";
import Header from "../../components/Header";
import Movie from "../../components/Movie";
import { fetchMovies } from "../../store/actions/movie";
import { connect } from "react-redux";
import { Container, Grid, Typography, withStyles } from "@material-ui/core";
import Page from "../../components/Page";
import styles from "./style";

class Home extends Component {
  render() {
    console.log(this.props.movies.totalCount);
    const { items, totalCount, count } = this.props.movies;
    const { title } = this.props.classes;
    return (
      <div>
        <Container maxWidth="lg" style={{ marginTop: 30 }}>
          <Typography
            variant="h3"
            style={{
              color: "#da1d1d",
              fontSize: 50,
              fontWeight: "bold",
              textAlign: "center",
              paddingBottom: 30,
            }}
            className={title}
          >
            BOM TẤN MÙA MƯA!!!!!
          </Typography>
          <Grid container spacing={3}>
            {items &&
              items.map((movie) => {
                return (
                  <Grid key={movie.maPhim} item xs={12} sm={6} md={3}>
                    <Movie movie={movie} />
                  </Grid>
                );
              })}
          </Grid>
          <div style={{ marginTop: 20, width:"100%"}}>
            <Page totalPages={totalCount} count={count} />
          </div>
        </Container>
      </div>
    );
  }

  componentDidMount() {
    this.props.dispatch(fetchMovies(1));
  }
}

const mapStateToProps = (state) => {
  return {
    movies: state.movieReducer.movies,
  };
};
export default connect(mapStateToProps)(
  withStyles(styles, { withTheme: true })(Home)
);
