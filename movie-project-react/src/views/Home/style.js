
const styles = (theme) => {
  return {
    title: {
        animation: "$Blink 0.5s infinite"
    },

    "@keyframes Blink": {
      from: {
        opacity: 0,
      },
      to: {
        opacity: 1,
      },
    },
  };
};

export default styles;
