import React, { Component, Fragment } from "react";
import { fetchMovie } from "../../store/actions/movie";
import { connect } from "react-redux";
import { Button, Container, Grid, Typography } from "@material-ui/core";
import { NfcSharp, Toys } from "@material-ui/icons";
import Schedule from "../../components/Schedule";
import Pending from "../../components/Pending";

class Details extends Component {
  render() {
    console.log("trong render nè cbị bóc nè", this.props.movie);

    if (!this.props.movie)
      return (
        <div
          style={{
            width: "100%",
            height: "800px",
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
          }}
        >
            <Pending />
        </div>
      ); // hiu cho nay hognnếu ko có j thì trả về  khoảng trắng hả

    // dung roi

    // hoac co cach khac nua

    // hoac dung default value ko hiu {}

    // boi vi ban dau this.porps.movie thif laf null
    // null theo falsy laf false ddungs ko
    // khi false thif laasy 1 casi object roongx
    // khi ddos minh destrucuring 1 object roongx thif nos bij undefined
    // khi nos bij undefined thif no nhaan defaule value laf ''

    // nhưng mà sao lúc nó chạy dc ? bài e-learning làm giống thầy á nó cũng ko báo null

    // chac chan co gi do khac

    // neu rong thi chac chan ko ra
    const {
      hinhAnh = "",
      biDanh = "",
      maPhim = "",
      lichChieu = "",
      ngayKhoiChieu = "",
      tenPhim = "",
      trailer = "",
      moTa = "",
      danhGia = "",
    } = this.props.movie || {};

    console.log("lich ne", lichChieu);
    return (
      <Container>
        <div style={{ display: "flex", margin: "auto", marginTop: 50 }}>
          {/* hình ảnh */}
          <Grid container style={{ width: "45%", marginRight: 10 }}>
            <Grid item>
              <img src={hinhAnh} style={{ width: "100%" }} />
            </Grid>
            {/* nút trailer */}
            <Button
              variant="contained"
              color="secondary"
              fullWidth
              style={{ marginTop: 10 }}
              href={trailer}
            >
              Xem Trailer
            </Button>
          </Grid>

          <div style={{ width: "45%", marginLeft: 20, lineHeight: 2 }}>
            {/* ten phim */}
            <Typography
              variant="h4"
              style={{
                color: "#dd1717d6",
                fontWeight: "bold",
                marginBottom: 10,
              }}
            >
              {tenPhim}
            </Typography>
            <Typography>Ngày khởi chiếu: {ngayKhoiChieu}</Typography>
            <Typography style={{ lineHeight: 2 }}>{moTa}</Typography>
            <p>Rating: {danhGia}/10</p>
          </div>
        </div>
        {/* bảng lịch chiếu */}
        <div className="mt-5">
          <Typography
            variant="h5"
            style={{
              color: "#dd1717d6",
              fontSize: 30,
              fontWeight: 500,
              padding: 10,
            }}
          >
            LỊCH CHIẾU
          </Typography>
          <Schedule lichChieu={lichChieu} />
        </div>
      </Container>
    );
  }

  componentDidMount() {
    console.log(this.props.match);
    const maPhim = this.props.match.params.id;
    this.props.dispatch(fetchMovie(maPhim));
  }
}

const mapStateToProps = (state) => {
  console.log("mapstate nè", state.movieReducer.movieDetails);
  return {
    movie: state.movieReducer.movieDetails,
  };
};

export default connect(mapStateToProps)(Details);
