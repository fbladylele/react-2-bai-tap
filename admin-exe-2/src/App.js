import React, { Component } from "react";
import { Route, BrowserRouter, Switch } from "react-router-dom";
import Home from "./views/Home";
import Dashboard from "./views/Dashboard";
import {AuthRoute} from "./HOCs/Route"

class App extends Component {
  render() {
    return (
      <BrowserRouter>
        <Switch>
          <Route exact path="/" component={Home} />
          <AuthRoute path="/admin" component={Dashboard} redirectPath="/" />
        </Switch>
      </BrowserRouter>
    );
  }
}
export default App;
