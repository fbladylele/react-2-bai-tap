//dashboard
import { Container, makeStyles, Typography } from "@material-ui/core";
import React, { useEffect, useState } from "react";
import List from "../../components/List";
import { useDispatch, useSelector } from "react-redux";
import * as yup from "yup";
import { useFormik } from "formik";
import axios from "axios";
import Pages from "../../components/Pages";
import { FetchSearchResult, fetchUserList } from "../../store/actions/userList";
import swal from "sweetalert";
import SearchIcon from "@material-ui/icons/Search";

const useStyles = makeStyles((theme) => ({
  margin: {
    marginTop: 50,
    marginBottom: 80,
  },
  heading: {
    marginTop: 30,
    marginBottom: -40,
    textAlign: "center",
    fontSize: 60,
    color: "#007bff",
  },
  middle: {
    display: "flex",
    justifyContent: "space-between",
  },
  pagination: {
    width: "100%",
    display: "flex",
    justifyContent: "end",
    padding: 30,
    paddingRight: 0,
  },
  input: {
    marginTop: 30,
    width: "45%",
    height: 30,
    position: "relative",
  },
  icon: {
    color: "#6d767e",
    height: 30,
    position: "absolute",
    right: 30,
    top: 5,
  },
}));

const schema = yup.object().shape({
  taiKhoan: yup.string().required("Vui lòng nhập!"),
  matKhau: yup.string().required("Vui lòng nhập!"),
  email: yup.string().required("Vui lòng nhập!"),
  maLoaiNguoiDung: yup.string().required("Vui lòng nhập!"),
  hoTen: yup.string().required("Vui lòng nhập!"),
  soDt: yup
    .string()
    .required("Vui lòng nhập!")
    .matches(/^[0-9]+$/g, "Vui lòng chỉ nhập chữ số!"),
});

const Dashboard = () => {
  const classes = useStyles();
  const dispatch = useDispatch();

  const formik = useFormik({
    initialValues: {
      taiKhoan: "",
      matKhau: "",
      email: "",
      soDt: "",
      maNhom: "GP01",
      maLoaiNguoiDung: "",
      hoTen: "",
    },
    validationSchema: schema,
    validateOnMount: true,
  });

  /**START SEARCH */
  let keywords = "";
  let timer = null;
  const handleChange = (event) => {
    keywords = event.target.value;
    console.log(event.target.value);

    if (timer) {
      clearTimeout(timer);
    }

    timer = setTimeout(() => {
      if (keywords) {
        dispatch(FetchSearchResult(1, keywords));
      } else {
        dispatch(fetchUserList(1));
      }
    }, 500);
  };
  /**END SEARCH */

  /** STARTS USER UPDATE*/
  /**@List: Edit Button: */
  const [isDisabled, setIsDisabled] = useState(false);
  const [edittedUser, setEdittedUser] = useState(null);
  const [isUpdate, setIsUpdadte] = useState(false);

  let pickedUser = "";

  const handleEdittedUser = (chosenUser) => {
    setEdittedUser(chosenUser);
    pickedUser = { ...chosenUser, maNhom: "GP01" };
    console.log(pickedUser);

    formik.setValues(pickedUser);
    setIsUpdadte(true);
    setIsDisabled(true);
  };

  /**@Dashboard: Update Button: */

  const handleUpdate = async (e) => {
    e.preventDefault();
    console.log(formik.values);

    formik.setTouched({
      taiKhoan: true,
      matKhau: true,
      email: true,
      soDt: true,
      maLoaiNguoiDung: true,
      hoTen: true,
    });

    if (!formik.isValid) return;

    try {
      let res = await axios({
        method: "POST",
        url: "http://movieapi.cyberlearn.vn/api/QuanLyNguoiDung/CapNhatThongTinNguoiDung",
        data: formik.values,
        headers: {
          Authorization: "Bearer " + localStorage.getItem("accessToken"),
        },
      });
      console.log(res.data);
      swal("Cập nhật User thành công!", "", "success");

      dispatch(fetchUserList(userPackage.currentPage));
    } catch (err) {
      console.log({ ...err });
      alert("error!");
    }

    formik.setTouched({
      taiKhoan: false,
      matKhau: false,
      email: false,
      soDt: false,
      maLoaiNguoiDung: false,
      hoTen: false,
    });
    formik.setValues(formik.initialValues);
    setIsUpdadte(false);
    setIsDisabled(false);
  };
  /**END USER UPDATE */

  useEffect(() => {
    dispatch(fetchUserList(1));
  }, []);

  const userPackage = useSelector((state) => {
    return state.listReducer.userPackage;
  });

  /**START ADD USERS */
  const handleAddUser = async (event) => {
    event.preventDefault();
    console.log("values adduser", formik.values);
    console.log("keywords:", formik.values.keywords);

    formik.setTouched({
      taiKhoan: true,
      matKhau: true,
      email: true,
      soDt: true,
      maLoaiNguoiDung: true,
      hoTen: true,
    });

    if (!formik.isValid) return;

    //start call api--------
    try {
      const res = await axios({
        url: "http://movieapi.cyberlearn.vn/api/QuanLyNguoiDung/ThemNguoiDung",
        method: "POST",
        data: formik.values,
        headers: {
          Authorization: "Bearer " + localStorage.getItem("accessToken"),
        },
      });
      console.log(res.data);
      swal("Công thành danh toại!", "", "success");
    } catch (err) {
      console.log({ ...err });
      swal("Fail nha", {
        button: false,
      });
    }
    //end call api ---------

    dispatch(fetchUserList(userPackage.currentPage));

    formik.setTouched({
      taiKhoan: false,
      matKhau: false,
      email: false,
      soDt: false,
      maLoaiNguoiDung: false,
      hoTen: false,
    });

    formik.setValues(formik.initialValues);
  };
  /**END ADD USERS */

  /**DELETE USERS */
  const deleteUser = async (username, currentPage) => {
    try {
      const res = await axios({
        url: "http://movieapi.cyberlearn.vn/api/QuanLyNguoiDung/XoaNguoiDung",
        method: "DELETE",
        params: {
          TaiKhoan: username,
        },
        headers: {
          Authorization: "Bearer " + localStorage.getItem("accessToken"),
        },
      });
      console.log(res.data);
      swal("Xóa thành công!", "", "success");
      dispatch(fetchUserList(currentPage));
    } catch (err) {
      console.log({ ...err });
      alert(err.response.data.message);
    }
  };

  return (
    <div>
      <Container maxWidth="lg">
        <Typography className={classes.heading} variant="h3">
          QUẢN LÝ NGƯỜI DÙNG
        </Typography>

        {/* ------- USERINPUT------- */}
        <div className={classes.margin}>
          <Container maxWidth="md">
            <form onSubmit={handleAddUser}>
              <div className="form-group">
                <label htmlFor="inputAddress">Tài Khoản</label>
                <input
                  value={formik.values.taiKhoan}
                  onBlur={formik.handleBlur}
                  name="taiKhoan"
                  onChange={formik.handleChange}
                  type="text"
                  className="form-control"
                  id="inputAddress"
                  disabled={isDisabled}
                />
                {formik.touched.taiKhoan && (
                  <p className="text-danger">{formik.errors.taiKhoan}</p>
                )}
              </div>

              <div className="form-group">
                <label htmlFor="inputAddress2">Mật khẩu </label>
                <input
                  value={formik.values.matKhau}
                  onBlur={formik.handleBlur}
                  name="matKhau"
                  onChange={formik.handleChange}
                  type="password"
                  className="form-control"
                  id="inputAddress2"
                />
                {formik.touched.matKhau && (
                  <p className="text-danger">{formik.errors.matKhau}</p>
                )}
              </div>

              <div className="form-group">
                <label htmlFor="inputAddress2">Họ Tên</label>
                <input
                  value={formik.values.hoTen}
                  onBlur={formik.handleBlur}
                  name="hoTen"
                  onChange={formik.handleChange}
                  type="text"
                  className="form-control"
                  id="inputAddress2"
                />
                {formik.touched.hoTen && (
                  <p className="text-danger">{formik.errors.hoTen}</p>
                )}
              </div>

              <div className="form-group">
                <label htmlFor="inputAddress2"> Email </label>
                <input
                  value={formik.values.email}
                  onBlur={formik.handleBlur}
                  name="email"
                  onChange={formik.handleChange}
                  type="email"
                  className="form-control"
                  id="inputAddress2"
                />
                {formik.touched.email && (
                  <p className="text-danger">{formik.errors.email}</p>
                )}
              </div>

              <div className="form-group">
                <label htmlFor="inputAddress2">Số điện thoại </label>
                <input
                  value={formik.values.soDt}
                  onBlur={formik.handleBlur}
                  name="soDt"
                  onChange={formik.handleChange}
                  type="text"
                  className="form-control"
                  id="inputAddress2"
                />
                {formik.touched.soDt && (
                  <p className="text-danger">{formik.errors.soDt}</p>
                )}
              </div>

              <div className="form-row">
                <div className="form-group col-md-6">
                  <label htmlFor="inputEmail4">Loại người dùng</label>
                  <input
                    value={formik.values.maLoaiNguoiDung}
                    onBlur={formik.handleBlur}
                    name="maLoaiNguoiDung"
                    onChange={formik.handleChange}
                    type="text"
                    className="form-control"
                    id="inputEmail4"
                    placeholder="QuanTri, KhachHang"
                  />
                  {formik.touched.maLoaiNguoiDung && (
                    <p className="text-danger">
                      {formik.errors.maLoaiNguoiDung}
                    </p>
                  )}
                </div>

                <div className="form-group col-md-6">
                  <label htmlFor="inputPassword4">Nhóm</label>
                  <input
                    readOnly
                    className="form-control"
                    id="inputPassword4"
                    placeholder="GP01"
                  />
                </div>
              </div>
              <div>
                {!isUpdate ? (
                  <button
                    type="submit"
                    className="btn btn-primary btn-lg font-weight-bold"
                  >
                    THÊM NGƯỜI DÙNG
                  </button>
                ) : (
                  <button
                    onClick={handleUpdate}
                    type="button"
                    className="btn btn-primary btn-lg font-weight-bold "
                  >
                    Cập nhật
                  </button>
                )}
              </div>
            </form>
          </Container>
        </div>

        <section className={classes.middle}>
          {/* ------- SEARCH ------- */}
          <div className={classes.input}>
            <div className="form-outline">
              <input
                name="keywords"
                onChange={handleChange}
                type="search"
                id="form1"
                className="form-control"
                placeholder="Tìm kiếm tên người dùng..."
              />
            </div>
            <SearchIcon className={classes.icon} />
          </div>

          {/*------- PAGINATION ------- */}
          <div className={classes.pagination}>
            {userPackage && (
              <Pages totalPages={userPackage.totalPages} keywords={keywords} />
            )}
          </div>
        </section>

        {/* -------LIST------- */}
        <div className={classes.margin}>
          <table className="table">
            <thead className="thead-light">
              <tr>
                <th scope="col">#</th>
                <th scope="col">Tài Khoản</th>
                <th scope="col">Nhóm</th>
                <th scope="col">Loại</th>
                <th scope="col">Họ tên</th>
                <th scope="col">Mật Khẩu</th>
                <th scope="col">Email</th>
                <th scope="col" className="text-center">
                  Sửa
                </th>
                <th scope="col" className="text-center">
                  Xóa
                </th>
              </tr>
            </thead>
            <tbody>
              {userPackage &&
                userPackage.items.map((item, i) => {
                  return (
                    <List
                      item={item}
                      i={i}
                      handleEdittedUser={handleEdittedUser}
                      deleteUser={deleteUser}
                      currentPage={userPackage.currentPage}
                    />
                  );
                })}
            </tbody>
          </table>
        </div>
      </Container>
    </div>
  );
};
export default Dashboard;

//http://movieapi.cyberlearn.vn/swagger/index.html?fbclid=IwAR332C1KAvtmbrwAfnU9Y-fwhfVI1uVaodR-SjqNVJWIDDYJOEsbzUjzZvo
