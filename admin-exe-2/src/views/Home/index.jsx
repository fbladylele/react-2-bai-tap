import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import TextField from "@material-ui/core/TextField";
import Grid from "@material-ui/core/Grid";
import AccountCircle from "@material-ui/icons/AccountCircle";
import VpnKeyIcon from "@material-ui/icons/VpnKey";
import { Button, Container, Typography } from "@material-ui/core";
import { useFormik } from "formik";
import * as yup from "yup";
import { signIn } from "../../store/actions/auth";
import { useDispatch } from "react-redux";

const schema = yup.object().shape({
  taiKhoan: yup.string().required("Vui lòng nhập tài khoản!"),
  matKhau: yup.string().required("Vui lòng nhập mật khẩu!"),
});

const useStyles = makeStyles((theme) => ({
  margin: {
    margin: "auto",
    width: "100%",
    height: 800,
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  center: {
    display: "flex",
    justifyContent: "center",
    marginTop: 10,
  },
  btn: {
    margin: "auto",
    textAlign: "center",
    padding: 10,
  },
}));

const Home = (props) => {
  const classes = useStyles();
  const dispatch = useDispatch();

  const formik = useFormik({
    initialValues: { taiKhoan: "", matKhau: "" },
    validationSchema: schema,
    validateOnMount: true,
  });

  const handleSubmit = (e) => {
    e.preventDefault();
    console.log(formik.values);

    formik.setTouched({
      taiKhoan: true,
      matKhau: true,
    });
    if (!formik.isValid) return;

    dispatch(
      signIn(formik.values, () => {
        props.history.push("/admin");
      })
    );
  };

  const handleDefaultUser = () => {
    const defaultUser = {
      taiKhoan: "adam",
      matKhau: "1",
    };
    formik.setValues(defaultUser);
  };

  return (
    <Container maxWidth="lg" className={classes.margin}>
      <div>
        <Typography
          className={classes.center}
          variant="h4"
          component="h1"
          color="primary"
        >
          QUẢN LÝ NGƯỜI DÙNG
        </Typography>
        <form onSubmit={handleSubmit}>
          <Grid
            className={classes.center}
            container
            spacing={1}
            alignItems="flex-end"
          >
            <Grid item>
              <AccountCircle color="disabled" fontSize="small" />
            </Grid>
            <Grid tem>
              <TextField
                value={formik.values.taiKhoan}
                name="taiKhoan"
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
                id="input-with-icon-grid"
                label="Tài Khoản"
              />
            </Grid>
            {formik.touched.taiKhoan && (
              <p className="text-danger">{formik.errors.taiKhoan}</p>
            )}
          </Grid>

          <Grid
            className={classes.center}
            container
            spacing={1}
            alignItems="flex-end"
          >
            <Grid item>
              <VpnKeyIcon color="disabled" fontSize="small" />
            </Grid>
            <Grid item>
              <TextField
                value={formik.values.matKhau}
                name="matKhau"
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
                id="input-with-icon-grid"
                label="Mật Khẩu"
                type="password"
              />
            </Grid>
            {formik.touched.matKhau && (
              <p className="text-danger">{formik.errors.matKhau}</p>
            )}
          </Grid>
          <div className={classes.btn}>
            <Button
              type="submit"
              color="primary"
              size="small"
              variant="contained"
              style={{ marginTop: 25 }}
            >
              Đăng nhập
            </Button>
            <Button
              type="button"
              onClick={handleDefaultUser}
              color="primary"
              size="small"
              variant="outlined"
              style={{
                marginTop: 25,
                marginLeft: 5,
                textTransform: "capitalize",
              }}
            >
              người dùng mặc định
            </Button>
          </div>
        </form>
      </div>
    </Container>
  );
};

export default Home;
