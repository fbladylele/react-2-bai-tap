import React from "react";
import { MemoryRouter, Route } from "react-router";
import { Link } from "react-router-dom";
import Pagination from "@material-ui/lab/Pagination";
import PaginationItem from "@material-ui/lab/PaginationItem";
import { FetchSearchResult, fetchUserList } from "../../store/actions/userList";
import { useDispatch } from "react-redux";

const Pages = (props) => {
  const dispatch = useDispatch();
  const handleChangePage = (event, value) => {
    if (!props.keywords) {
      dispatch(fetchUserList(value));
    } else {
      dispatch(FetchSearchResult(value, props.keywords));
    }
  };
  return (
    <MemoryRouter initialEntries={["/inbox"]} initialIndex={0}>
      <Route>
        {({ location }) => {
          const query = new URLSearchParams(location.search);
          const page = parseInt(query.get("page") || "1", 10);
          return (
            <Pagination
              onChange={handleChangePage}
              count={props.totalPages}
              renderItem={(item) => (
                <PaginationItem
                  component={Link}
                  to={`/inbox${item.page === 1 ? "" : `?page=${item.page}`}`}
                  {...item}
                />
              )}
            />
          );
        }}
      </Route>
    </MemoryRouter>
  );
};

export default Pages;
