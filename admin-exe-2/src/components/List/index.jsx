import { Button } from "@material-ui/core";
import React from "react";
import DeleteIcon from "@material-ui/icons/Delete";
import EditIcon from '@material-ui/icons/Edit';


const List = (props) => {
  
  return (
    <tr key={props.item.taiKhoan}>
      <td>{props.i + 1}</td>
      <td>{props.item.taiKhoan}</td>
      <td>{props.item.maNhom ? props.item.maNhom : "N/A"}</td>
      <td>{props.item.maLoaiNguoiDung}</td>
      <td>{props.item.hoTen}</td>
      <td>{props.item.matKhau}</td>
      <td>{props.item.email}</td>
      <td>
        <Button onClick={()=>props.handleEdittedUser(props.item)} type="button" style={{ outline: "none" }}>
          <EditIcon color="action" fontSize="medium" />
        </Button>
      </td>
      <td>
        <Button onClick={()=>props.deleteUser(props.item.taiKhoan, props.currentPage)} type="button" style={{ outline: "none" }}>
          <DeleteIcon color="action " />
        </Button>
      </td>
    </tr>
  );
};

export default List;
