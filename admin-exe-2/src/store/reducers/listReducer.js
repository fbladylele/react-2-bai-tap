import { act } from "react-dom/test-utils";
import { actionType } from "../actions/type";

const initialState = {
  userPackage: null,
};

const reducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case actionType.FETCH_LIST:
      state.userPackage = payload;
      return { ...state };

    default:
      return state;
  }
};
export default reducer;
