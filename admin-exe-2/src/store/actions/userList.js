import axios from "axios";
import { createAction } from ".";
import { actionType } from "./type";

export const fetchUserList = (page) => {
  return async (dispatch) => {
    try {
      const res = await axios({
        url: "http://movieapi.cyberlearn.vn/api/QuanLyNguoiDung/LayDanhSachNguoiDungPhanTrang",
        method: "GET",
        params: {
          maNhom: "GP01",
          soTrang: page,
          soPhanTuTrenTrang: 10,
        },
      });
      console.log(res.data.content);

      dispatch(createAction(actionType.FETCH_LIST, res.data.content));
    } catch (err) {
      console.log({ ...err });
    }
  };
};

//for Search:
export const FetchSearchResult = (page, keywords) => {
  return async (dispatch) => {
    try {
      const res = await axios({
        url: "http://movieapi.cyberlearn.vn/api/QuanLyNguoiDung/TimKiemNguoiDungPhanTrang",
        method: "GET",
        params: {
          MaNhom: "GP01",
          tuKhoa: keywords,
          soTrang: page,
          soPhanTuTrenTrang: 10,
        },
      });
      console.log(res.data);

      dispatch(createAction(actionType.FETCH_LIST, res.data.content))

    } catch (err) {
      console.log({...err});
      alert("something wrong with search")
    }
  };
};
