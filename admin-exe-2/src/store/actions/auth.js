import axios from "axios";
import { createAction } from ".";
import {actionType} from "./type";
import swal from 'sweetalert';

export const signIn = (values, callback) => {
  return async (dispatch) => {
    try {
      const res = await axios({
        url: "http://movieapi.cyberlearn.vn/api/QuanLyNguoiDung/DangNhap",
        method: "POST",
        data: values,
      });
      console.log(res.data);
      swal("Xin chào! Chúc một ngày tốt lành", {
        buttons: false,
      });

      dispatch(createAction(actionType.SET_ME, res.data));

      localStorage.setItem("accessToken", res.data.content.accessToken);

      callback();

    } catch (err) {
        console.log({...err});
        swal("awww, rất tiếc bạn không thể đăng nhập", {
          button: false,
        });
    }
  };
};
